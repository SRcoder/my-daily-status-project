package com.sr.dailystatus.models;

import lombok.Data;

import java.util.Date;

/**
 * Created by Sandeep Rana on 29-May-18.
 */
@Data
public class  Status {
    String id;
    String name;
    String development;
    String junit;
    String sonar;
    String nPE;
    String qc;
    Date entryDate;
}
