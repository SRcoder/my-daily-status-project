package com.sr.dailystatus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DailystatusApplication {

	public static void main(String[] args) {
		SpringApplication.run(DailystatusApplication.class, args);
	}
}
